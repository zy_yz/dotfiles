#!/bin/sh 

xrandr --output LVDS1 --off --output DP1 --off --output HDMI1 --off --output VGA1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output VIRTUAL1 --off &
polybar &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
imwheel &
picom &
nitrogen --restore &
