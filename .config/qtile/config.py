from typing import List  # noqa: F401

from libqtile import bar, layout, widget, hook, qtile
from libqtile.config import Click, Drag, Group, Key, Match, Screen, ScratchPad, DropDown
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

import os
import subprocess

mod = "mod4"
terminal = guess_terminal()

keys = [
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(),
        desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "control"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch default alacritty terminal"),
    Key([mod, "shift"], "Return", lazy.spawn('kitty'), desc="Launch kitty terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "d", lazy.spawn('rofi -show'), desc="Open rofi"),
    Key([mod, "shift"], "d", lazy.spawncmd(),
        desc="Spawn a command using a prompt widget"),
    Key([mod, "shift"], "z", lazy.spawn('slock'),
        desc="Spawn a command using a prompt widget"),
]

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
        #     desc="move focused window to group {}".format(i.name)),
    ])

layouts = [
    layout.Columns(
    	border_width = 2,
    	margin=[0, 12, 12, 12],
    	#margin=[0, 8, 8, 8],
    	#margin=0,
    	border_focus = "898d98",
    	border_unfocus = "e1acff"
    ),
    layout.Floating(float_rules = [{'wmclass':'alacritty'},]),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    layout.Stack(num_stacks=2),
    layout.Bsp(),
    layout.Matrix(),
    layout.MonadTall(),
    layout.MonadWide(),
    layout.RatioTile(),
    layout.Tile(),
    layout.TreeTab(),
    layout.VerticalTile(),
    layout.Zoomy(),
]

widget_defaults = dict(
    background = "#242424",
    font='roboto mono bold',
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.GroupBox(
                	center_aligned = 'true',
                	disable_drag = 'true',
                	highlight_method = "block",
                	active = "#ffffff",
                	this_current_screen_border = "#ffffff",
                	block_highlight_text_color = "#000000"
                ),
                widget.Sep(foreground = "#ffffff", padding = 8, size_percent = 60, linewidth = 1),
                widget.CurrentLayout(),
                widget.Prompt(),
                widget.Spacer(),
                widget.Net(
                	foreground = "#ffffff",
                	padding = 10,
                	format = '{down}↓↑{up}',
                	update_interval = 1
                ),
                widget.Sep(foreground = "#ffffff", padding = 8, size_percent = 60, linewidth = 1),
                widget.Volume(
                	padding = 10,
                	fmt = 'Vol: {}',
                	mouse_callbacks={'Button1':lambda: qtile.cmd_function(open_pavu)}
                ),
                widget.Sep(foreground = "#ffffff", padding = 8, size_percent = 60, linewidth = 1),
                widget.ThermalSensor(
                	foreground = "#ffffff",
                	padding = 10,
                	format = 'CPU {load_percent}%',
                	show_tag = 'true',
                	mouse_callbacks={'Button1':lambda: qtile.cmd_function(open_psensor)},
                	update_interval = 1
                ),
                widget.Sep(foreground = "#ffffff", padding = 8, size_percent = 60, linewidth = 1),
                widget.CPU(
                	foreground = "#ffffff",
                	padding = 10,
                	format = 'CPU {load_percent}%',
                	mouse_callbacks={'Button1':lambda: qtile.cmd_function(open_system_monitor)},
                	update_interval = 1
                ),
                widget.Sep(foreground = "#ffffff", padding = 8, size_percent = 60, linewidth = 1),
                widget.Clock(
                	#format='%H:%M:%S',
                	#format=' %H:%M:%S %p',
                	format=' %H:%M:%S ',
                	foreground = "#ffffff",
                	mouse_callbacks={'Button1':lambda: qtile.cmd_function(open_gsimplecal)},
                	margin =0,
                	padding = 0
                ),
                widget.Sep(foreground = "#ffffff", padding = 8, size_percent = 60, linewidth = 1),
                widget.TextBox(
			mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn("wlogout")},
			text = "[X]",
			foreground = "#ffffff",
			padding =5
		),
            ],
            24,
            opacity=1,
            margin=[12, 12, 12, 12],
            #margin=[8, 8, 8, 8],
            #margin=0,
            background="#222222"
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

# Add mouse_callback functions
def open_pavu(qtile):
    qtile.cmd_spawn("pavucontrol")
def open_psensor(qtile):
    qtile.cmd_spawn("psensor")
def open_system_monitor(qtile):
    qtile.cmd_spawn("gnome-system-monitor")
#@subscribe.client_mouse_enter
def open_gsimplecal(qtile):
    qtile.cmd_spawn("gsimplecal next_month")

# Auto start
@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([home])

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# Set the scratchpad
groups.append(ScratchPad('scrachpad', [
	DropDown('term', terminal, width=0.4, height=0.5, x=0.3, y=0.2, opacity=1),
]))

keys.extend([
	Key(["control"], "1", lazy.group['scratchpad'].dropdown_toggle('term')),
])

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
