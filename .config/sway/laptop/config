#############
#           #
# Variables #
#           #
#############

# Logo key. Use Mod1 for Alt.
set $mod Mod4
# Home row direction keys, like vim
set $left h
set $down j
set $up k
set $right l
# Your preferred terminal emulator
set $term alacritty
# Your preferred application launcher
# Note: pass the final command to swaymsg so that the resulting window can be opened
# on the original workspace that the command was run on.
set $menu dmenu_path | dmenu | xargs swaymsg exec --

#########################
#                       #
# Output configuration  #
#                       #
#########################

# Default wallpaper (more resolutions are available in /usr/share/backgrounds/sway/)
output * bg /home/locker/Images/hills.jpg fill

#
# Example configuration:
#

output eDP-1 resolution 1920x1080 position 1920,0

# You can get the names of your outputs by running: swaymsg -t get_outputs

### Idle configuration
#
# Example configuration:
#
# exec swayidle -w \
#          timeout 300 'swaylock -f -c 000000' \
#          timeout 600 'swaymsg "output * dpms off"' resume 'swaymsg "output * dpms on"' \
#          before-sleep 'swaylock -f -c 000000'
#
# This will lock your screen after 300 seconds of inactivity, then turn off
# your displays after another 300 seconds, and turn your screens back on when
# resumed. It will also lock your screen before your computer goes to sleep.

### Input configuration
#
# Example configuration:
#
#   input "2:14:SynPS/2_Synaptics_TouchPad" {
#       dwt enabled
#       tap enabled
#       natural_scroll enabled
#       middle_emulation enabled
#   }
#
# You can get the names of your inputs by running: swaymsg -t get_inputs
# Read `man 5 sway-input` for more information about this section.

#########################
#                       #
# Key bindings : Basics #
#                       #
#########################

# Start a terminal
bindsym $mod+Return exec $term
bindsym $mod+Shift+Return exec kitty

# Kill focused window
bindsym $mod+Shift+q kill

# Start your launcher
bindsym $mod+Shift+d exec $menu
bindsym $mod+d exec --no-startup-id rofi -show
bindsym $mod+Alt+Space exec ulauncher
bindsym Alt+Space exec tofi-drun

# Drag floating windows by holding down $mod and left mouse button.
# Resize them with right mouse button + $mod.
# Despite the name, also works for non-floating windows.
# Change normal to inverse to use left mouse button for resizing and right
# mouse button for dragging.
floating_modifier $mod normal

# Reload the configuration file
bindsym $mod+Shift+c reload

# Exit sway (logs you out of your Wayland session)
bindsym $mod+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'

################################
#                                                                        #
# Key bindings : Moving around                 #
#                                                                        #
################################

# Move your focus around
bindsym $mod+$left focus left
bindsym $mod+$down focus down
bindsym $mod+$up focus up
bindsym $mod+$right focus right

# Or use $mod+[up|down|left|right]
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# Move the focused window with the same, but add Shift
bindsym $mod+Shift+$left move left
bindsym $mod+Shift+$down move down
bindsym $mod+Shift+$up move up
bindsym $mod+Shift+$right move right

# Ditto, with arrow keys
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

#############################
#                           #
# Key bindings : Workspaces #
#                           #
#############################

# Switch to workspace
bindsym $mod+1 workspace number 1
bindsym $mod+2 workspace number 2
bindsym $mod+3 workspace number 3
bindsym $mod+4 workspace number 4
bindsym $mod+5 workspace number 5
bindsym $mod+6 workspace number 6
bindsym $mod+7 workspace number 7
bindsym $mod+8 workspace number 8
bindsym $mod+9 workspace number 9
bindsym $mod+0 workspace number 10

# Move focused container to workspace
bindsym $mod+Shift+1 move container to workspace number 1
bindsym $mod+Shift+2 move container to workspace number 2
bindsym $mod+Shift+3 move container to workspace number 3
bindsym $mod+Shift+4 move container to workspace number 4
bindsym $mod+Shift+5 move container to workspace number 5
bindsym $mod+Shift+6 move container to workspace number 6
bindsym $mod+Shift+7 move container to workspace number 7
bindsym $mod+Shift+8 move container to workspace number 8
bindsym $mod+Shift+9 move container to workspace number 9
bindsym $mod+Shift+0 move container to workspace number 10
# Note: workspaces can have any name you want, not just numbers.
# We just use 1-10 as the default.

###############################
#                             #
# Key bindings : Layout stuff #
#                             #
###############################

# You can "split" the current object of your focus with
# $mod+b or $mod+v, for horizontal and vertical splits
# respectively.
bindsym $mod+b splith
bindsym $mod+v splitv

# Switch the current container between different layout styles
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# Make the current focus fullscreen
bindsym $mod+f fullscreen

# Toggle the current focus between tiling and floating mode
bindsym $mod+Shift+space floating toggle

# Swap focus between the tiling area and the floating area
bindsym $mod+space focus mode_toggle

# Move focus to the parent container
bindsym $mod+a focus parent

#############################
#                           #
# Key bindings : Scratchpad #
#                           #
#############################

# Sway has a "scratchpad", which is a bag of holding for windows.
# You can send windows there and get them back later.

# Move the currently focused window to the scratchpad
bindsym $mod+Shift+minus move scratchpad

# Show the next scratchpad window or hide the focused scratchpad window.
# If there are multiple scratchpad windows, this command cycles through them.
bindsym $mod+minus scratchpad show

######################################
#                                    #
# Key bindings : Resizing containers #
#                                    #
######################################

mode "resize" {
    # left will shrink the containers width
    # right will grow the containers width
    # up will shrink the containers height
    # down will grow the containers height
    bindsym $left resize shrink width 10px
    bindsym $down resize grow height 10px
    bindsym $up resize shrink height 10px
    bindsym $right resize grow width 10px

    # Ditto, with arrow keys
    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px

    # Return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"

################################
#                              #
# Add waybar to the status bar #
#                              #
################################

bar {
    swaybar_command waybar
    #position top
    #gaps 10 10
    #colors {
        # Text color of status bar
        #statusline #ffffff
        # Background of status bar
        #background #2f343f
        
        #focused_workspace #8d8e91 #8d8e91 #000000
    #}
    #status_command while date +'%Y-%m-%d %I:%M:%S %p'; do sleep 1; done
}

############
#          #
# Add gaps #
#          #
############

# Add a gaps
gaps inner 5
gaps outer 5

####################
#                  #
# Remove title bar #
#                  #
####################

default_border pixel 2

##################
#                #
# Set gesettings #
#                #
##################

set $gnome-schema org.gnome.desktop.interface
exec_always {
    gsettings set $gnome-schema gtk-theme 'Flat-Remix-GTK-Green-Dark-Solid'
    gsettings set $gnome-schema icon-theme 'Flat-Remix-Green-Dark'
}

##################
#                #
# windows colors #
#                #
##################

client.focused          #898d98 #898d98 #898d98 #898d98   #898d98
client.focused_inactive #2f3137 #2f3137 #2f3137 #2f3137   #2f3137
client.unfocused        #2f3137 #2f3137 #2f3137 #2f3137   #2f3137
client.urgent           #ffffff #ffffff #000000 #ffffff   #ffffff
client.placeholder      #2f3137 #2f3137 #2f3137 #2f3137   #2f3137

###########################
# Auto start apps in sway #
###########################

exec /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
exec imwheel &

###################################################
# Activate SwayIdle and sway lock after some time #
###################################################

exec swayidle -w \
         timeout 900 'swaylock -i /home/locker/Images/table_paper_origami_airplanes_63593_1920x1080.jpg --inside-color 808080 --key-hl-color ffffff --ring-color 808080' \
        # timeout 1500 'swaymsg "output * dpms off"' resume 'swaymsg "output * dpms on"' \
         #before-sleep 'swaylock -i /home/locker/Images/table_paper_origami_airplanes_63593_1920x1080.jpg --inside-color 808080 --key-hl-color ffffff --ring-color 808080'

##############################
# Get trackpad configuration #
##############################

input "10182:480:DELL0A23:00_27C6:01E0_Touchpad" {
	dwt enabled
	tap enabled
	middle_emulation enabled
}

###############################
#                             #
# Include default config file #
#                             #
###############################

include /etc/sway/config.d/*
